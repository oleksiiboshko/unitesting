﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTesting.Interfaces
{
    public interface I2DFigure
    {
        double GetArea();
    }
}
