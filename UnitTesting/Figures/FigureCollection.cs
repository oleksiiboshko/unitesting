﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitTesting.Interfaces;

namespace UnitTesting.Figures
{
    public class FigureCollection
    {
        private List<I2DFigure> _figures;
        public List<I2DFigure> Figures 
        {
            get
            {
                if (_figures == null)
                {
                    _figures = new List<I2DFigure>();
                    return _figures;
                }
                return _figures;
            }
            set
            {
                _figures = value;
            }
        }
        
        public void AddFigure(I2DFigure figure)
        {
            Figures.Add(figure);
        }



        public double GetTotalArea()
        {
            var totalArea = 0d;
            foreach (var figure in Figures)
            {
                totalArea += figure.GetArea();
            }

            return totalArea;
        }
    }
}
