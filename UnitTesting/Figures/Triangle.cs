﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitTesting.Interfaces;

namespace UnitTesting.Figures
{
    public class Triangle: I2DFigure
    {
        public double Area { get; set; }

        public double GetArea()
        {
            return Area;
        }
    }
}
