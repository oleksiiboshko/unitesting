﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitTesting.Interfaces;

namespace UnitTesting.Figures
{
    public class Square: I2DFigure
    {
        public double Side { get; set; }

        public double GetArea()
        {
            return Side * Side;
        }
    }
}
