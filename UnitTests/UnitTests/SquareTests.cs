﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using UnitTesting.Figures;

namespace UnitTesting.UnitTests
{
    [TestFixture]
    public class SquareTests
    {
        [Test]
        public void GetArea_SetSideValue_ReturnArea()
        {
            //arrange
            var square = new Square { Side = 4d };
            var expectedArea = 16d;
            //act
            var actualArea = square.GetArea();
            //assert
            Assert.AreEqual(expectedArea, actualArea);
            
        }
    }
}
