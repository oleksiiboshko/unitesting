﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using NUnit.Framework;
using UnitTesting.Figures;
using UnitTesting.Interfaces;

namespace UnitTesting.UnitTests
{
    [TestFixture]
    public class FigureCollectionTest
    {
        [Test]
        public void AddFigure_AddsOneFigure_OneObjectInProperty() 
        {
            //arrange
            var figureCollection = new FigureCollection();
            var mockFigureToAdd = new Mock<I2DFigure>();
            mockFigureToAdd.Setup(x => x.GetArea()).Returns(4d);
            //act
            figureCollection.AddFigure(mockFigureToAdd.Object);
            var figureAmount = figureCollection.Figures.Count;
            //assert
            Assert.AreEqual(1, figureAmount);
        }

        [Test]
        public void GetTotalArea_AddsThreeFigures_ReturnsTotalArea()
        {
            //arrange
            var figureCollection = new FigureCollection();

            var firstMockFigureToAdd = new Mock<I2DFigure>();
            firstMockFigureToAdd.Setup(x => x.GetArea()).Returns(4d);

            var secondMockFigureToAdd = new Mock<I2DFigure>();
            firstMockFigureToAdd.Setup(x => x.GetArea()).Returns(4d);

            var thirdMockFigureToAdd = new Mock<I2DFigure>();
            thirdMockFigureToAdd.Setup(x => x.GetArea()).Returns(4d);

            var expectedTotalArea = 12d;
            //act
            figureCollection.AddFigure(firstMockFigureToAdd.Object);
            figureCollection.AddFigure(secondMockFigureToAdd.Object);
            figureCollection.AddFigure(thirdMockFigureToAdd.Object);

            var actualTotalArea = figureCollection.GetTotalArea();
            //assert
            Assert.AreEqual(expectedTotalArea,actualTotalArea);
        }
    }
}
