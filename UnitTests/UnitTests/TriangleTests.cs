﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using UnitTesting.Figures;

namespace UnitTesting.UnitTests
{
    [TestFixture]
    public class TriangleTests
    {
        [Test]
        public void GetGetArea_SetAreaValue_ReturnAreaArea()
        {
            //arrange
            var triangle = new Triangle { Area = 9d };
            var expectedArea = 9d;
            //act
            var actualArea = triangle.GetArea();
            //assert
            Assert.AreEqual(expectedArea, actualArea);
        }
    }
}
